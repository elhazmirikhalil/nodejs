const cluster = require('cluster');
if (cluster.isMaster) {
    console.log(`master process ${process.pid}`)
    const cpuCount = require('os').cpus().length;
    for (let i=0; i < cpuCount; i+=1) {
        cluster.();
    }
}
else{
    const http = require("http");
    const server = http.createServer(
        (req,res) => res.end("done")
    );
    server.listen(1337)
}